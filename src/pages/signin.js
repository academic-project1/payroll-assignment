import React from 'react'
import { Formik, Field, Form } from 'formik'
import './home.css';
import * as Yup from 'yup'
import { login } from '../utility';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Axios from 'axios';

const signinSchema = Yup.object().shape({
    Username: Yup.string()
        .required('Please enter your username')
        .matches(/(7|8|9)\d{9}$/, 'Enter Valid Username'),
    Password: Yup.string()
        .required('Please enter your password'),
});

function Signin() {
    let navigate = useNavigate();
    const handlesignin = async(values) => {
        try{
            const res = await Axios.post("/API/Account/authenticate", values)
            localStorage.setItem('IsActive',res.data.userDetail.data.IsActive)
            toast.success('User Logged in')
            navigate('/home')
        }catch(error){
            console.log('error', error)
            toast.error()
        }
    }
    return (
        <div className='signin-from'>
            <div className='inner-div'>
                <div className=''>
                    <div className=''>
                        <h3 style={{ color: '#7ad117' }}>Signin</h3>
                        <Formik initialValues={{
                            Username: '',
                            Password: ''
                        }}
                            validationSchema={signinSchema}
                            onSubmit={handlesignin}
                        >{({ errors, touched }) => (
                            <Form>
                                <div className='row form-row'>
                                    <Field type='text' id="email" name="Username" placeholder="Username" className='inputField' />
                                    {errors.Username && touched.Username ? (
                                        <div className='error'>{errors.Username}</div>
                                    ) : null}
                                </div>
                                <div className='row form-row'>
                                    <Field type='password' id="password" name="Password" placeholder="Password" className='inputField' />
                                    {errors.Password && touched.Password ? (
                                        <div className='error'>{errors.Password}</div>
                                    ) : null}
                                </div>
                                <div className='row row-btn'>
                                    <button type='submit' className='submitbtn'>SIGN IN</button>
                                </div>
                            </Form>
                        )}
                        </Formik>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Signin