import React, { useState, useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'

import Basket from '../component/basket'
import Header from '../component/header'
import './home.css'
import AddItem from '../modals/addItem'
import { getData } from '../utility'

function Home() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const [data, setData] = useState()
  const initial = {
    id: '',
    name: '',
    description: '',
    amount: '',
    filename: '',
    quantity: ''
  }
  const [foodData, setFoodData] = useState(initial);
  useEffect(() => {
    getdata();
  }, [])

  const getdata = async () => {
    await getData("/food")
      .then(response => setData(response.data))
      .catch(error => console.log(error))
  }

  const handleData = (row) => {
    setShow(true)
    setFoodData({
      id: row.id,
      name: row.name,
      description: row.description,
      amount: row.amount,
      filename: row.filename,
      quantity: row.quantity
    })
  }


  return (
    <>
      <Header />
      <div className='main-container'>
        <div className='bundle' id='bundle'>
          <h4>Bundles</h4>
          <Row className='bundlerow'>
            <Col lg={8} className="col8">
              {data && data.map((rows) => (
                <Row key={rows.id} className='inner-row'>
                  {rows.bundle && rows.bundle.map((row) => (
                    <Col lg={6} key={row.id} className="bundlescol">
                      <div className='fooditem-box'>
                        <Row className='onclicks' onClick={() => handleData(row)}>
                          <Col lg={8}>
                            <span className='bundle-title'>{row && row.name}</span>
                            <p className='foodDescription'>{row && row.description}</p>
                            <label className='amount'>&#8377;{row && row.amount}</label>
                          </Col>
                          <Col lg={4}>
                            <img src={row && row.filename} alt="image" className="imgclass" />
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  ))}
                </Row>
              ))}
            </Col>
            <Col lg={4} className='col4'>
              <Basket />
            </Col>
          </Row>
        </div>

        <div className='starters' >
          <h4 id='starters'>Starters</h4>
          <Row>
            <Col lg={8} className="col8">
              {data && data.map((rows) => (
                <Row key={rows.id} className='inner-row'>
                  {rows.starters && rows.starters.map((row) => (
                    <Col lg={6} key={row.id} className="bundlescol">
                      <div className='fooditem-box'>
                        <Row className='onclicks' onClick={() => handleData(row)}>
                          <Col lg={8}>
                            <span className='bundle-title'>{row && row.name}</span>
                            <p className='foodDescription'>{row && row.description}</p>
                            <label className='amount'>&#8377;{row && row.amount}</label>
                          </Col>
                          <Col lg={4}>
                            <img src={row && row.filename} alt="image" className="imgclass" />
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  ))}
                </Row>
              ))}
            </Col>
          </Row>
        </div>

        <div className='main-dishes' id='main-dishes'>
          <h4>Main Dishes</h4>
          <Row>
            <Col lg={8} className="col8">
              {data && data.map((rows) => (
                <Row key={rows.id} className='inner-row'>
                  {rows.main_dishes && rows.main_dishes.map((row) => (
                    <Col lg={6} key={row.id} className="bundlescol">
                      <div className='fooditem-box'>
                        <Row className='onclicks' onClick={() => handleData(row)}>
                          <Col lg={8}>
                            <span className='bundle-title'>{row && row.name}</span>
                            <p className='foodDescription'>{row && row.description}</p>
                            <label className='amount'>&#8377;{row && row.amount}</label>
                          </Col>
                          <Col lg={4}>
                            <img src={row && row.filename} alt="image" className="imgclass" />
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  ))}
                </Row>
              ))}
            </Col>
          </Row>
        </div>

        <div className='sides' id='sides'>
          <h4>Sides</h4>
          <Row>
            <Col lg={8} className="col8">
              {data && data.map((rows) => (
                <Row key={rows.id} className='inner-row'>
                  {rows.sides && rows.sides.map((row) => (
                    <Col lg={6} key={row.id} className="bundlescol">
                      <div className='fooditem-box'>
                        <Row className='onclicks' onClick={() => handleData(row)}>
                          <Col lg={8}>
                            <span className='bundle-title'>{row && row.name}</span>
                            <p className='foodDescription'>{row && row.description}</p>
                            <label className='amount'>&#8377;{row && row.amount}</label>
                          </Col>
                          <Col lg={4}>
                            <img src={row && row.filename} alt="image" className="imgclass" />
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  ))}
                </Row>
              ))}
            </Col>
          </Row>
        </div>

        <div className='desserts' id='desserts'>
          <h4>Desserts</h4>
          <Row>
            <Col lg={8} className="col8">
              {data && data.map((rows) => (
                <Row key={rows.id} className='inner-row'>
                  {rows.dessert && rows.dessert.map((row) => (
                    <Col lg={6} key={row.id} className="bundlescol">
                      <div className='fooditem-box'>
                        <Row className='onclicks' onClick={() => handleData(row)}>
                          <Col lg={8}>
                            <span className='bundle-title'>{row && row.name}</span>
                            <p className='foodDescription'>{row && row.description}</p>
                            <label className='amount'>&#8377;{row && row.amount}</label>
                          </Col>
                          <Col lg={4}>
                            <img src={row && row.filename} alt="image" className="imgclass" />
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  ))}
                </Row>
              ))}
            </Col>
          </Row>
        </div>
      </div>
      {show ?
        <AddItem
          show={show}
          hide={handleClose}
          item={foodData}
        /> : ''}
    </>
  )
}

export default Home