import React, { useState } from 'react'
import './component.css'
import { Navbar, Nav, Container } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import { AiOutlineMenu } from 'react-icons/ai'

function Header() {

  const navigate = useNavigate()
  function logout() {
    localStorage.removeItem('IsActive');
    navigate('/')
  }
  const [activetab, setActivetab] = useState('#bundle');

  const navtitle = [
    {
      name: "Bundles",
      href: "#bundle",
    },
    {
      name: "Starters",
      href: "#starters",
    },
    {
      name: "Main Dishes",
      href: "#main-dishes",
    },
    {
      name: "Sides",
      href: "#sides",
    },
    {
      name: "Desserts",
      href: "#desserts",
    },

  ]
  return (
    <div className=''>
      {/* <Navbar className='header'>
          <Nav className="me-auto">
            <div className='header-row'>
              {navtitle.map((data, index) => (

                <Nav.Link href={data.href} active={activetab === data.href} key={index} onClick={() => setActivetab(data.href)}>{data.name}</Nav.Link>
              ))}
              <Nav.Link onClick={logout} className='logout'>Logout</Nav.Link>
            </div>
          </Nav>
        </Navbar> */}
      <Navbar bg="white" expand="lg">
        <Container fluid>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll" className='header-nav'>
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: '200px' }}
              navbarScroll
            >
            {navtitle.map((data, index) => (

              <Nav.Link href={data.href} active={activetab === data.href} key={index} onClick={() => setActivetab(data.href)}>{data.name}</Nav.Link>
            ))}
            <Nav.Link onClick={logout} className='logout'>Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

export default Header