import React, { useContext, useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap';
import { MdDeleteForever } from 'react-icons/md'
import { deleteData, getData } from '../utility';
import './component.css';

import { context } from '../App'

function Basket() {
    const { reload, setReload } = useContext(context)
    const [cartdetail, setCartdetail] = useState();
    const getFoodData = async () => {
        const cart = await getData("/addtoCart")
        setCartdetail(cart);
    }

    useEffect(() => {
        getFoodData();
    }, [reload])

    const totalAmt = () => {
        const initialValue = 0;
        let amount = cartdetail && cartdetail.data.map((data) => data.totalamount)
        let bundleAmount;
        if (amount != null) {
            bundleAmount = amount && amount.reduce((prev, current) => prev + current, initialValue)
        }

        let amounts = cartdetail && cartdetail.data.map((data) => data.extraDish != null ?
            (data.extraDish.map((row) => row.amount * row.quantity)) : null)
        let extraDishAmount;
        let extraAmt = []
        if (amounts != null) {
            for (let i = 0; amounts.length > i; i++) {
                if (amounts[i] != null) {
                    extraDishAmount = amounts && amounts[i].reduce((prev, current) => prev + current, initialValue);
                    extraAmt.push(extraDishAmount)
                }
            }
        }
        let total;
        if (extraAmt != null) {
            total = extraAmt.reduce((prev, current) => prev + current, initialValue)
        }
        return bundleAmount + total;
    }

    const handleDelete = (id, name) => {
        console.log('id', id)
        if(name === 'bundle'){
            deleteData(`/addtoCart/${id}`)
        }else{
            deleteData(`/addtoCart/extraDish/${id}`)
        }
        setReload(pre => (!pre))
    }
    return (
        <div className='basket-box'>
            <div className=''>
                <Row>
                    <Col>
                        <span className='title-bs'>Your basket</span>
                    </Col>
                    <Col>
                        <span className='titleorder'>Order</span>
                    </Col>
                </Row>
                <Row>
                    <p style={{ fontSize: '13px', color: '#9e9d9d' }}>From<span style={{ color: '#7ad117', fontWeight: 'bolder' }}> St.Petit Bistrot</span></p>
                </Row>
            </div>
            <div>
                {cartdetail && cartdetail.data.length > 0 ? (cartdetail.data.map((row) => (
                    <Row key={row.id} className='items-block'>
                        <Row className="cartrow">
                            <Col lg={9} className="bundleName">{row.bundlename}</Col>
                            <Col lg={3} className='amount'>&#8377;{row.amount}
                                <MdDeleteForever className='closeIcon' onClick={() => handleDelete(row.prod_id, 'bundle')} />
                            </Col>
                        </Row>
                        {row.firstPizza !== null ?
                            <Row className="cartrow">
                                <Col lg={9} className="cart-data">{row.firstPizza}</Col>
                                <Col lg={3} className='amount'>&#8377;{row.firstPizzaAmt}</Col>
                            </Row> : ''}
                        {row.secondPizza !== null ?
                            <Row className="cartrow">
                                <Col lg={9} className="cart-data">{row.secondPizza}</Col>
                                <Col lg={3} className='amount'>&#8377;{row.secondPizzaAmt}</Col>
                            </Row> : ''}
                        {row.extraDish !== null ?
                            row.extraDish && row.extraDish.map((rows) => (
                                <Row key={rows.id}>
                                    <Col lg={9} className="cart-data">{rows.name}</Col>
                                    <Col lg={3} className='amount'>&#8377;{rows.amount * rows.quantity}
                                        <MdDeleteForever className='closeIcon' onClick={() => handleDelete(rows.id, 'extradish')} />
                                    </Col>
                                </Row>
                            ))
                            : ''}
                        {row.notes != null ?
                            <Row>
                                <Col className="bundleName">Notes:{row.notes}</Col>
                            </Row> : ''
                        }
                    </Row>
                ))) : (<center><label className='noData'>No added items yet</label></center>)
                }
                {cartdetail?.data.length === 0 ?
                    <Row style={{ padding: '0 12px' }}>
                        <button className='check-btn'>CONTINUE TO CHECKOUT</button>
                    </Row> :
                    <Row style={{ padding: '0 12px' }} className='checkRow'>
                        <button className='checkout'>CHECKOUT<label className='checkoutAmt'>&#8377;{totalAmt()}</label></button>
                    </Row>
                }
            </div>
        </div>
    )
}

export default Basket
