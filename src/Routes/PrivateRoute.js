import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

function PrivateRoute() {
    const isActive = localStorage.getItem('IsActive')
  return ( isActive ? <Outlet/> :<Navigate to="/"/>
  )
}

export default PrivateRoute