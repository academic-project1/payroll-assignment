import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

function PublicRoute() {
    const isActive = localStorage.getItem('IsActive')
  return ( !isActive ?<Outlet/>:<Navigate to="/home"/>
  )
}

export default PublicRoute