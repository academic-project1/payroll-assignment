import { request } from "../request/request"

export const postData=(url,body)=>{
  return  request.post(url,body)
}

export const getData=(url)=>{
  return request.get(url)
}

export const deleteData=(url)=>{
  return request.delete(url)
}