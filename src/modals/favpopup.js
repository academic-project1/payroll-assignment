import React, { useEffect, useState } from 'react'
import { Modal, Row, Col, Tab, Nav } from 'react-bootstrap'
import { GrClose } from 'react-icons/gr'
import { AiOutlinePlusCircle } from 'react-icons/ai'
import { AiOutlineMinusCircle } from 'react-icons/ai'
import { AiOutlineArrowLeft } from 'react-icons/ai'

import { getData } from '../utility';
import './modals.css';

function Favpopup({ favPop, hide, getFavoritedata, setExtraDish, setNote }) {
    const [ids, setIds] = useState();
    const [favData, setFavData] = useState();
    const [extraData, setExtraData] = useState();
    const [notes, setNotes] = useState();
    useEffect(() => {
        getFavdata();
        getExtraDish();
    }, [])

    const getFavdata = async () => {
        await getData("/favorites")
            .then(response => setFavData(response.data))
            .catch(error => console.log(error))
    }

    const getExtraDish = async () => {
        const { data } = await getData("/extra")
        setExtraData(data);

    }

    let data = extraData && extraData.filter(row => row.isActive === true);

    const handleClick = () => {
        getFavoritedata(ids)
        setExtraDish(data)
        setNote(notes)
        hide()
    }


    const handleCheckbox = (e, rows) => {
        for (let i = 0; extraData.length > i; i++) {
            if (extraData[i].id == rows.id) {
                if (e.target.checked) {
                    extraData[i].isActive = true;
                } else {
                    extraData[i].isActive = false;
                }
            }
        }
        setExtraData([...extraData])
    }

    const handleQty = (rows, name) => {
        for (let i = 0; extraData.length > i; i++) {
            if (extraData[i].id == rows.id) {
                if (name === "incre") {
                    extraData[i].quantity += 1;
                } else if (name === "decre" && rows.quantity != 1) {
                    extraData[i].quantity -= 1;
                }
            }
        }
        setExtraData([...extraData])
    }


    return (
        <div>
            <Modal show={favPop} onHide={hide}>
                <div className='header-divs'>
                    <button onClick={hide} className='back-btn'>
                        <AiOutlineArrowLeft className='close-icon'/>
                    </button>
                    <h4 className='modal_title'>Choose Your Favorite</h4>
                    <button onClick={hide} className='close-btns'>
                        <GrClose className='close-icon'/>
                    </button>
                </div>
                <div className='modal_body'>
                    <Tab.Container id="left-tabs-example" defaultActiveKey="pizza">
                        <Nav variant="pills" className="flex-row">
                            <Nav.Item>
                                <Nav.Link eventKey="pizza" className='option-label'>Pizza</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="extra" className='option-label'>Extra</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        <Tab.Content>
                            <Tab.Pane eventKey="pizza">
                                <div className='pizza-block'>
                                    <div className='fav-title'>
                                        <label className='favTxt'>Choose max. 1 items</label>
                                    </div>
                                    <div className='menulist'>
                                        {favData && favData.map((data) => (
                                            <Row className='favlist-row' key={data.id} onClick={() => setIds(data.id)}>
                                                <Col lg={9} className='col9'>
                                                    <span className='itemtitle'>{data.name}</span>
                                                    <p className='foodDescription'>{data.description}</p>
                                                </Col>
                                                <Col lg={3} className="col3">
                                                    <img src={data.filename} alt="image" className="favImg" />
                                                </Col>
                                            </Row>
                                        ))}
                                    </div>
                                </div>
                            </Tab.Pane>
                            <Tab.Pane eventKey="extra">
                                <div className='extra-block'>
                                    <div className='fav-title'>
                                        <label></label>
                                    </div>
                                    <div className='menulist'>
                                        {extraData && extraData.map((rows) =>
                                            <Row className='favlist-row' key={rows.id}>
                                                <Col lg={8}>
                                                    <label className='extra-label'>{rows.name}</label>
                                                </Col>
                                                <Col lg={4}>
                                                    <div className='checkBox'>
                                                        {rows.isActive === true ?
                                                            <>
                                                                <AiOutlineMinusCircle className='plusminusicon' onClick={() => handleQty(rows, "decre")}
                                                                // quantity != 1 && setQuantity(quantity - 1)}
                                                                />
                                                                <span className='count'>{rows.quantity}</span>
                                                                <AiOutlinePlusCircle className='plusminusicon' onClick={() => handleQty(rows, "incre")}
                                                                //  setQuantity(quantity + 1)}
                                                                />
                                                            </>
                                                            : <label className='amt'>&#8377;{rows.amount}</label>}
                                                        &nbsp;
                                                        <span><input type='checkbox' className='input-checkbox' checked={rows.isActive}
                                                            onChange={(e) => handleCheckbox(e, rows)} /></span>
                                                    </div>
                                                </Col>
                                            </Row>
                                        )}
                                    </div>
                                    <div className='fav-title'>
                                        <label className='notesTxt'>Additional Notes</label>
                                    </div>
                                    <div className='notesBox'>
                                        <input type="text" placeholder='e.g. no pickles' className='input-text'
                                            onChange={(e) => setNotes(e.target.value)} />
                                    </div>
                                    <div className='fav-title'><br /><br /><br /> <br></br></div>
                                </div>
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </div>
                <div className='footer-div'>
                    <div className=''>
                        <Row>
                            <Col>
                                <button className={ids === undefined ? 'basket-btn' : 'basket-btnss'} onClick={handleClick}>ADD TO BASKET</button>
                            </Col>
                        </Row>
                    </div>
                </div>
            </Modal>
        </div>
    )
}


export default Favpopup