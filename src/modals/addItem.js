import React, { useContext, useEffect, useState } from 'react'
import { Modal, Row, Col } from 'react-bootstrap'
import { GrClose } from 'react-icons/gr'
import { AiOutlinePlusCircle } from 'react-icons/ai'
import { AiOutlineMinusCircle } from 'react-icons/ai'

import './modals.css';
import { getData, postData } from '../utility'
import Favpopup from './favpopup'
import { context } from '../App'

function AddItem({ show, hide, item }) {
    const {reload, setReload} = useContext(context)
    const [favPop, setFavPop] = useState(false);
    const handleClose = () => setFavPop(false);
    const [info, setInfo] = useState();

    const [qty, setQty] = useState(1);

    const [pizzaone, setPizzaone] = useState(null);
    const [pizzatwo, setPizzatwo] = useState(null);

    const [extraDish, setExtraDish] = useState();
    const [note, setNote] =useState();

    const postdata = () => {
        const initial = {
            prod_id: item?.id,
            bundlename: item?.name,
            description: item?.description,
            firstPizza: pizzaone !== null ? pizzaone.name : null,
            firstPizzaAmt: pizzaone !== null ? pizzaone.amount : null,
            secondPizza: pizzatwo !== null ? pizzatwo.name : null,
            secondPizzaAmt: pizzatwo !== null ? pizzatwo.amount : null,
            amount: item?.amount,
            quantity: qty,
            totalamount: qty * item?.amount + (pizzaone !== null ? pizzaone.amount : null)+(pizzatwo !== null ? pizzatwo.amount : null),
            extraDish:extraDish,
            notes:note
        }
        postData("/addtoCart", initial)
        setReload(pre=>!pre)
        hide()
    }

    const getFavoritedata = async (id) => {
        const { data } = await getData(`/favorites/${id}`)
        if (info === 'first') {
            setPizzaone(data);
        } else {
            setPizzatwo(data);
        }
    }

    useEffect(() => {
        return () => {
            setQty(1);
            setPizzaone(null);
            setPizzatwo(null)
        }
    }, [reload])

    const price = () => {
        if (pizzaone !== null && pizzatwo !== null) {
            return (item?.amount + pizzaone?.amount + pizzatwo?.amount) * qty
        } else if (pizzaone !== null) {
            return (item?.amount + pizzaone?.amount) * qty
        } else if (pizzatwo !== null) {
            return (item?.amount + pizzatwo?.amount) * qty
        } else {
            return item?.amount * qty
        }
    }
    return (
        <div>
            <Modal show={show} onHide={hide}>
                <div className='header-div'>
                    <h4 className='modal_title'>Add Item To Basket</h4>
                    <button onClick={hide} className='close-btn'>
                        <GrClose  className='close-icon'/>
                    </button>
                </div>
                <div className='modal_body'>
                    <img src={item?.filename} alt="image" className="imgstyle" />
                    <div className='titlesection'>
                        <Row>
                            <Col>
                                <h5 style={{ fontWeight: 700 }}>{item?.name}</h5>
                            </Col>
                            <Col style={{ textAlign: 'right' }}>
                                <label className='inr'>&#8377;{item?.amount}</label>
                            </Col>
                        </Row>
                        <Row>
                            <p className='food_info'>{item?.description}</p>
                        </Row>
                    </div>
                    <div className='fav-title'>
                        <label className='fav_txt'>Choose Your Favorites</label>
                    </div>
                    <div className='chooseClass'>
                        <div className='item-label' onClick={() => { setFavPop(true); setInfo('first') }}>
                            {pizzaone === null ?
                                <label className='choicelabel'>Choose Your first pizza</label>
                                : <Row>
                                    <Col lg={10}>
                                        <label className='selectedlabel'>{pizzaone.name}</label><br />
                                        <label className='description'>{pizzaone.description.slice(0, 30) + "..."}</label>
                                    </Col>
                                    <Col lg={2} className="inr_col"><span className='favoriteAmt'>&#8377;{pizzaone.amount}</span></Col>
                                </Row>
                            }
                        </div>
                        <div className='item-label' onClick={() => { setFavPop(true); setInfo('second') }}>
                            {pizzatwo === null ?
                                <label className='choicelabel'>Choose Your first pizza</label>
                                : <Row>
                                    <Col lg={10}>
                                        <label className='selectedlabel'>{pizzatwo.name}</label><br />
                                        <label className='description'>{pizzatwo.description.slice(0, 30) + "..."}</label>
                                    </Col>
                                    <Col lg={2} className="inr_col"><span className='favoriteAmt'>&#8377;{pizzatwo.amount}</span></Col>
                                </Row>
                            }
                        </div>
                    </div>
                </div>
                <div className='footer-div'>
                    <div className=''>
                        <Row>
                            <Col lg={4}>
                                <div className='countItem'>
                                    <AiOutlineMinusCircle disabled className='minusicon' onClick={() => qty != 1 && setQty(qty - 1)} />
                                    <span>{qty}</span>
                                    <AiOutlinePlusCircle className='plusicon' onClick={() => setQty(qty + 1)} />
                                </div>
                            </Col>
                            <Col lg={8}>
                                <button className='addBasket-btn' onClick={postdata}>ADD TO BASKET </button>
                                <span className='addBasketInr'>&#8377;{price()}</span>
                            </Col>
                        </Row>
                    </div>
                </div>
            </Modal>
            {favPop &&
                <Favpopup
                    favPop={favPop}
                    hide={handleClose}
                    info={info}
                    getFavoritedata={getFavoritedata}
                    setExtraDish={setExtraDish}
                    setNote={setNote} />
            }
        </div>
    )
}


export default AddItem