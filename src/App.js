import './App.css';
import Home from './pages/home';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.css'
import Signin from './pages/signin';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import PrivateRoute from './Routes/PrivateRoute';
import PublicRoute from './Routes/PublicRoute';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { createContext, useState } from 'react';
export const context = createContext()

function App() {
  const [reload, setReload] = useState(false)

  return (
    <>
    <context.Provider value={{reload,setReload}}>
      <ToastContainer/>
        <BrowserRouter>
          <Routes>
            <Route element={<PublicRoute />}>
              <Route exact path='/' element={<Signin />} />
            </Route>
            <Route element={<PrivateRoute />}>
              <Route exact path='/home' element={<Home />} />
            </Route>
          </Routes>
        </BrowserRouter>
    </context.Provider>
    </>
  );
}

export default App;
